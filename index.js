var express = require('express');
var mysql=require('mysql');
var bodyParser = require('body-parser')
var config = require('./config.json');
var pool  = mysql.createPool({
  host     : config.dbhost,
  user     : config.dbuser,
  password : config.dbpassword,
  database : config.dbname
});
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Credentials", "true");
  res.header("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, X-Auth-Token"
  );
  next();
});

app.get('/', function (req, res) {
    pool.getConnection(function (err, connection) {
        // Use the connection
        connection.query('SELECT * from std', function (error, results, fields) {
            // And done with the connection.
            connection.release();
            // Handle error after the release.
            if (error) throw error;
            else console.log(results);
            res.send(results);
            // process.exit();
        });
    });
});
app.listen(8080)

module.exports = app;

